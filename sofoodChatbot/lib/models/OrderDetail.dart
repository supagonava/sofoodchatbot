class OrderDetail {
  int orderId, quantity, price;
  String name;
  OrderDetail({this.orderId, this.quantity, this.price, this.name});
}
