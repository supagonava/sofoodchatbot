import 'dart:convert' as convert;

import 'package:flutter/cupertino.dart';
import '../appConfig.dart';
import 'package:http/http.dart' as http;

class Product {
  int id, price, category, score;
  String name, description, image;

  static List<Product> prodLst = [];
  static List<Product> duplicateList = [];

  static Future getAllProd() async {
    prodLst.clear();
    duplicateList.clear();
    var res = await http.get(AppConfig.getAllProdApi);
    var lst = convert.jsonDecode(res.body);
    print(lst['data']);
    if (lst['status'] == 200 && lst['data'] != null) {
      if (lst['data'].length > 0) {
        lst['data'].forEach((prod) {
          prodLst.insert(
              0,
              Product(
                  id: prod['id'],
                  score: 5,
                  name: prod['name'],
                  image: prod['path'],
                  description: prod['description'],
                  price: prod['price'],
                  category: prod['category']));
        });
      }
    }

    duplicateList = prodLst;
  }

  Product(
      {@required this.id,
      @required this.name,
      @required this.image,
      @required this.description,
      @required this.price,
      @required this.score,
      @required this.category});
}
