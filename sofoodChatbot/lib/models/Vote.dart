import 'package:chatbot/appConfig.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class Vote {
  final int id;
  final int score;
  final String reviewTxt, reviewerName, date;

  Vote(this.id, this.reviewerName, this.score, this.reviewTxt, this.date);

  static Future<List> getVote(prodId) async {
    var res = await http.get(AppConfig.host + "vote/get/" + prodId.toString());
    print(res.body);
    List voteLst = [];
    if (res.statusCode == 200) {
      var response = convert.jsonDecode(res.body);
      if (response['data'] != null) {
        var lst = response['data'] as List;
        lst.forEach((element) {
          voteLst.insert(
              0,
              Vote(element['id'], element['firstname'], element['score'],
                  element['review'], "ไม่ระบุ"));
        });
      }
    }
    return voteLst;
  }
}
