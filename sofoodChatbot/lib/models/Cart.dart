import 'package:chatbot/appConfig.dart';
import 'package:chatbot/models/Account.dart';
import 'package:chatbot/models/Product.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class Cart {
  Product prod;
  int quantity, id;

  Cart({@required this.prod, @required this.quantity, @required this.id});

  static List<Cart> cartList = [];

  static Future getCart() async {
    cartList.clear();
    var res = await http
        .get("http://202.183.198.250:5000/cart/user/" + Account.id.toString());
    if (res.statusCode == 200) {
      var list = convert.jsonDecode(res.body);
      var datalst = list['data'] as List;
      datalst.forEach((element) {
        // print(element);
        Cart.cartList.insert(
            0,
            Cart(
                prod: Product(
                    id: element['prod_id'],
                    name: element['name'],
                    image: element['path'],
                    description: element['description'],
                    price: element['price'],
                    score: 5,
                    category: element['category']),
                quantity: element['quantity'],
                id: element['id']));
      });
      return true;
    } else {
      return false;
    }
  }

  static Future pushCart(prodId, quantity) async {
    Map data = {
      "user_id": Account.id,
      "action": "push cart",
      "product_id": prodId
    };
    http.post("http://202.183.198.250/admin/myadmin/web/site/action-log",
        headers: AppConfig.header, body: convert.jsonEncode(data));

    var res = await http.post(AppConfig.pushToCartApi, headers: {
      "prod_id": prodId,
      "quantity": quantity,
      "user_id": Account.id.toString()
    });
    if (res.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  static Future removeCart(id) async {
    var res = await http.delete(AppConfig.host + "cart/" + id.toString());
    print(AppConfig.host + "cart/" + id.toString());
    print(res.body);
  }
}
