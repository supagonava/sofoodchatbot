import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import '../appConfig.dart';
import 'Account.dart';

class ChatMessage extends StatelessWidget {
  ChatMessage({this.text, this.name, this.type, this.widget});

  final String text;
  final String name;
  final bool type;
  final Widget widget;

  static List<ChatMessage> chatLst = [];

  static getOnlineMessage() async {
    chatLst.clear();
    var res = await http.post(
        "http://202.183.198.250/admin/myadmin/web/site/get-chat",
        headers: AppConfig.header,
        body: convert.jsonEncode({"user_id": Account.id}));
    print("online message = " + res.body);
    var response = convert.jsonDecode(res.body);
    if (response['data'] != null) {
      var lst = response['data'] as List;
      lst.forEach((element) {
        chatLst.insert(
            0,
            ChatMessage(
              name: "คุณ",
              text: element['chat_message'],
              type: true,
              widget: null,
            ));
        chatLst.insert(
            0,
            ChatMessage(
              name: "น้องจูเลียต",
              text: element['chat_response'],
              type: false,
              widget: null,
            ));
      });
    }
  }

  List<Widget> botMessage(context) {
    return <Widget>[
      Container(
        margin: const EdgeInsets.only(left: 15.0, right: 20),
        child: CircleAvatar(
          backgroundColor: Theme.of(context).primaryColor,
          child: Image.asset(
            "assets/images/icon.png",
            height: 35,
          ),
          minRadius: 20,
        ),
      ),
      Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(this.name,
                style: TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.black87)),
            Container(
              margin: const EdgeInsets.only(top: 5.0),
              child: Column(
                children: [
                  Text(
                    text,
                    style: TextStyle(fontSize: 16, color: Colors.black87),
                  ),
                  widget != null ? widget : SizedBox()
                ],
              ),
              padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: Color.fromRGBO(255, 206, 41, 0.8),
              ),
            ),
          ],
        ),
      ),
    ];
  }

  List<Widget> myMessage(context) {
    return <Widget>[
      Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Text(this.name,
                style: TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.black87)),
            Container(
              margin: const EdgeInsets.only(top: 5.0),
              child: Text(
                text,
                style: TextStyle(fontSize: 16, color: Colors.black87),
              ),
              padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: Color.fromRGBO(255, 206, 41, 1),
              ),
            ),
          ],
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: this.type ? myMessage(context) : botMessage(context),
      ),
    );
  }
}
