import 'package:chatbot/appConfig.dart';
import 'package:chatbot/models/Cart.dart';
import 'package:chatbot/models/OrderDetail.dart';

import '../models/Account.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

class Order {
  int id, sum;
  String shippingAddress;
  List<OrderDetail> detail = [];
  static List<Order> orderLst = [];

  Order({this.id, this.sum, this.shippingAddress, this.detail});

  static Future<List<OrderDetail>> getDetail(id) async {
    var res1 = await http.get("http://202.183.198.250:5000", headers: {
      "command":
          "SELECT * FROM order_detail Left Join myorder on myorder.id = order_detail.order_id Left Join product on product.id = order_detail.product_id where order_detail.order_id = $id"
    });
    var response1 = convert.jsonDecode(res1.body);
    List<OrderDetail> detailLst = [];
    if (response1['data'] != null) {
      response1['data'].forEach((e1) {
        detailLst.insert(
            0,
            OrderDetail(
                name: e1['name'],
                orderId: e1['id'],
                price: e1['price'],
                quantity: e1['quantity']));
      });
    }
    return detailLst;
  }

  static Future getOrder() async {
    orderLst.clear();
    var res = await http.get("http://202.183.198.250:5000", headers: {
      "command":
          "SELECT *,(SELECT sum(price) FROM order_detail WHERE order_id = myorder.id GROUP BY order_id) as sum FROM myorder where user_id = ${Account.id}"
    });
    var response = convert.jsonDecode(res.body);
    if (response['data'] != null) {
      response['data'].forEach((e) {
        getDetail(e['id']).then((value) {
          orderLst.insert(
              0,
              Order(
                  id: e['id'],
                  shippingAddress: e['shipping_address'],
                  sum: int.parse(e['sum']),
                  detail: value));
        });
      });
    }
    return orderLst;
//SELECT *,(SELECT sum(price) FROM order_detail WHERE order_id = myorder.id GROUP BY order_id) as sum FROM myorder
//SELECT * FROM myorder LEFT JOIN order_detail on order_detail.order_id = myorder.id
  }

  static makeOrder() async {
    String sql =
        "INSERT INTO myorder (user_id,status,shipping_address) VALUES (${Account.id},1,(SELECT users.province FROM users WHERE users.id = ${Account.id}));";
    var res = await http
        .put("http://202.183.198.250:5000", headers: {"command": sql});
    print(res.body);
    Cart.cartList.forEach((element) async {
      sql =
          "INSERT INTO order_detail (order_id,product_id,quantity,price) VALUES ((SELECT id FROM myorder WHERE myorder.user_id = ${Account.id} ORDER BY id DESC LIMIT 1),${element.prod.id},${element.quantity},${element.quantity * element.prod.price});";
      res = await http
          .put("http://202.183.198.250:5000", headers: {"command": sql});

      Map data = {
        "user_id": Account.id,
        "action": "make order",
        "product_id": element.prod.id
      };
      res = await http.post(
          "http://202.183.198.250/admin/myadmin/web/site/action-log",
          headers: AppConfig.header,
          body: convert.jsonEncode(data));
      print(res.body);
    });
    Cart.removeCart(Account.id);
  }
}
