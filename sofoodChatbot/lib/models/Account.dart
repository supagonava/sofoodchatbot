import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../screens/Homepage.dart';
import 'dart:convert' as convert;
import '../appConfig.dart';
import '../page_route.dart';

class Account {
  static int point, id;
  static String province,
      gender,
      firstname,
      lastname,
      dob,
      phone,
      career,
      username,
      password;

  static void reLogin(BuildContext context) {
    Navigator.of(context)
        .pushAndRemoveUntil(MyPageRoute(widget: HomePage()), (route) => false);
  }

  static Future<bool> checkLogin() async {
    await AppConfig.setToken();
    var res = await http
        .get(AppConfig.decryptTokenApi, headers: {"token": AppConfig.token});
    if (res.statusCode == 200 && !res.body.contains("Error")) {
      print("loged in");
      var response = convert.jsonDecode(res.body);
      if (response['isLogin']) {
        id = response['data']['id'];
        firstname = response['data']['firstname'];
        lastname = response['data']['lastname'];
        dob = response['data']['date_of_birth'];
        gender = response['data']['gender'];
        career = response['data']['career'];
        phone = response['data']['phone'];
        province = response['data']['province'];
        username = response['data']['username'];
        password = response['data']['password'];
        point = response['data']['point'];
        return true;
      }
      return false;
    }
    return false;
  }

  static signOut(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('token');
    Account.career = null;
    Account.id = null;
    Account.username = null;
    Account.password = null;
    Account.firstname = null;
    Account.lastname = null;
    Account.dob = null;
    Account.gender = null;
    Account.phone = null;
    Account.point = null;
    Account.province = null;
    AppConfig.token = null;
    AppConfig.token = prefs.getString("token");
    reLogin(context);
    Fluttertoast.showToast(msg: "ออกจากระบบแล้ว");
  }

  static Future<bool> signIn(String username, String password) async {
    var res = await http.get(
      AppConfig.signinApi,
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'username': username,
        'password': password
      },
    );
    if (res.statusCode == 200) {
      var response = convert.jsonDecode(res.body);
      if (response['token'] != null) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("token", response['token']);
        AppConfig.token = response['token'];
        Fluttertoast.showToast(msg: "เข้าสู่ระบบแล้ว");
        return true;
      }
    } else {
      Fluttertoast.showToast(msg: "ชื่อผู้ใช้หรือรหัสผ่านผิดค่ะ");
    }
    return false;
    // print(res.body);
  }

  static updatePoint() async {
    var res = await http.put(AppConfig.host,
        headers: {"command": "update users set point = $point where id = $id"});
    print(res.body);
  }
}
