import 'package:chatbot/models/Account.dart';
import 'package:flutter/material.dart';
import 'dart:math';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController usernameTxtController = TextEditingController();
  TextEditingController passwordTxtController = TextEditingController();

  List<String> username = [
    'poram',
    'mixLnwZa',
    'tleLnwZaa',
    'slivsey0',
    'lgossop1',
    'gkeattch2',
    'pchaikovski3',
    'gcuttler4',
    'fhecks5',
    'cdrought6',
    'phaskins7'
  ];
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  width: width,
                  alignment: Alignment.topLeft,
                  color: const Color(0xffffb90f),
                  child: FlatButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: Icon(Icons.arrow_back)),
                ),
                Container(
                  width: width,
                  height: 300.0,
                  decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.only(bottomLeft: Radius.circular(100)),
                    color: const Color(0xffffb90f),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/images/icon.png",
                        width: width * 0.4,
                      ),
                      Text(
                        "Login",
                        style: TextStyle(fontSize: 25),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 80,
                ),
                Container(
                  width: width * 0.8,
                  height: 80,
                  child: TextField(
                    controller: usernameTxtController,
                    maxLength: 20,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.account_circle),
                        labelText: "Username..",
                        border: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 0.5,
                              color: Color(0xff707070),
                            ),
                            borderRadius: BorderRadius.circular(25))),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: width * 0.8,
                  height: 80,
                  child: TextField(
                    controller: passwordTxtController,
                    obscureText: true,
                    maxLength: 20,
                    keyboardType: TextInputType.visiblePassword,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.vpn_key),
                        labelText: "Password..",
                        border: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 0.5,
                              color: Color(0xff707070),
                            ),
                            borderRadius: BorderRadius.circular(25))),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                ButtonTheme(
                  minWidth: width * 0.8,
                  height: 50,
                  child: FlatButton.icon(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25)),
                      color: Theme.of(context).primaryColor,
                      splashColor: Colors.white,
                      onPressed: () async {
                        Account.signIn(usernameTxtController.text,
                                passwordTxtController.text)
                            .then((value) {
                          if (value) {
                            Account.reLogin(context);
                          }
                        });
                      },
                      icon: Icon(Icons.arrow_forward_ios),
                      label: Text("Signin")),
                ),
                SizedBox(
                  height: 10,
                ),
                ButtonTheme(
                  minWidth: width * 0.8,
                  height: 50,
                  child: FlatButton.icon(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25)),
                      color: Theme.of(context).primaryColor,
                      splashColor: Colors.white,
                      onPressed: () {
                        final _random = new Random();
                        int randomNum = _random.nextInt(username.length - 1);
                        usernameTxtController.text = username[randomNum];
                        passwordTxtController.text = '123456';
                      },
                      icon: Icon(Icons.account_box),
                      label: Text("SignUp")),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
