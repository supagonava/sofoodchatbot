import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:chatbot/components/EndDrawer.dart';
import 'package:chatbot/components/FloatBtn.dart';
import 'package:chatbot/models/Account.dart';
import 'package:chatbot/models/Product.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../appConfig.dart';
import '../components/Waiting.dart';
import 'LoginPage.dart';
import 'ProdView.dart';
import 'Cartpage.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController txtC = TextEditingController();
  int indexStack = 0;
  final List<String> bannerLst = [
    'https://i.ibb.co/tpwbw6t/115824997-287740619162014-3822775994726599775-n.png',
    'https://i.ibb.co/ngYQ8rQ/Sofood.png',
  ];
  var categoryLst = [
    'ทั้งหมด',
    'อาหารคาว',
    'ขนม',
    'เครื่องดื่ม',
    "ยารักษา",
    "ของใช้ส่วนตัว"
  ];
  var selected = 0;

  @override
  void initState() {
    super.initState();
    Product.getAllProd().then((_) {
      Account.checkLogin().then((value) {
        setState(() {
          indexStack = 1;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    var prodList = Wrap(
      direction: Axis.horizontal,
      alignment: WrapAlignment.center,
      spacing: 1,
      runSpacing: 1,
      children: !(Product.prodLst == null)
          ? Product.prodLst
              .map((model) => Container(
                    width: width * 0.48,
                    height: 300,
                    color: Colors.white,
                    child: FlatButton(
                      splashColor: Theme.of(context).primaryColor,
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => ProdView(
                                  product: model,
                                )));
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Image.network(
                            (() {
                              try {
                                return model.image;
                              } catch (e) {
                                print(e);
                                return "https://i.ibb.co/bspSz2j/generic-error.png";
                              }
                            }()),
                            width: width * 0.4,
                            height: 200,
                            fit: BoxFit.cover,
                          ),
                          Wrap(
                            direction: Axis.horizontal,
                            children: List.generate(
                                model.score,
                                (index) => Icon(
                                      Icons.star,
                                      color: Color(0xffF8DA48),
                                      size: 16,
                                    )),
                          ),
                          Text(
                            '${model.name}',
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontSize: 12,
                              color: const Color(0xff303030),
                            ),
                            textAlign: TextAlign.left,
                          ),
                          Text(
                            '${model.price} บาท',
                            style: TextStyle(
                              fontFamily: 'Prompt',
                              fontSize: 12,
                              color: const Color(0xff303030),
                              fontWeight: FontWeight.w700,
                            ),
                            textAlign: TextAlign.left,
                          )
                        ],
                      ),
                    ),
                  ))
              .toList()
          : null,
    );

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        floatingActionButton: FloatBtn(),
        endDrawer: EndDrawer(),
        appBar: AppBar(
          title: Wrap(
            direction: Axis.horizontal,
            children: [
              Image.asset(
                "assets/images/icon.png",
                width: width * 0.15,
              ),
              Container(
                  margin: EdgeInsets.only(top: 5),
                  width: width * 0.35,
                  height: 45,
                  child: TextField(
                    controller: txtC,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      hintText: 'ค้นหา',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20)),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(25.7),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(25.7),
                      ),
                    ),
                  )),
              ButtonTheme(
                minWidth: width * 0.1,
                child: FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  onPressed: () {
                    setState(() {
                      Product.prodLst = Product.duplicateList
                          .where((element) => element.name.contains(txtC.text))
                          .toList();
                    });
                  },
                  child: Icon(Icons.search),
                ),
              ),
              ButtonTheme(
                minWidth: width * 0.1,
                child: FlatButton(
                  child: Icon(Icons.shopping_cart),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) =>
                            Account.username != null && AppConfig.token != null
                                ? MyCart()
                                : LoginPage()));
                  },
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25)),
              )
            ],
          ),
        ),
        body: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: IndexedStack(
            index: indexStack,
            children: [
              Waiting(),
              SingleChildScrollView(
                child: Column(
                  children: [
                    CarouselSlider(
                      options: CarouselOptions(
                        enableInfiniteScroll: true,
                        reverse: false,
                        autoPlay: true,
                        autoPlayInterval: Duration(seconds: 3),
                        autoPlayAnimationDuration: Duration(milliseconds: 800),
                        autoPlayCurve: Curves.fastOutSlowIn,
                        enlargeCenterPage: true,
                      ),
                      items: bannerLst
                          .map((item) => Container(
                                child: Image.network(item,
                                    fit: BoxFit.cover, width: width),
                              ))
                          .toList(),
                    ),
                    WarantyBanner(),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'หมวดหมู่',
                        style: TextStyle(
                          fontFamily: 'Prompt',
                          fontSize: 16,
                          color: const Color(0xff646464),
                          fontWeight: FontWeight.w700,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Wrap(
                      spacing: 5,
                      direction: Axis.horizontal,
                      alignment: WrapAlignment.spaceEvenly,
                      children: categoryLst.map((e) {
                        var typeBtnColor = Theme.of(context).primaryColor;

                        var textColor = Colors.white;

                        return FlatButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12),
                                side: BorderSide(width: 0.5)),
                            onPressed: () {
                              // print(categorylcategoryLst

                              //     .indexWhere((element) => element.contains(e)));

                              setState(() {
                                selected = categoryLst.indexWhere(
                                    (element) => element.contains(e));

                                print(selected);

                                if (selected == 0) {
                                  Product.prodLst = Product.duplicateList;
                                } else {
                                  Product.prodLst =
                                      Product.duplicateList.where((element) {
                                    return element.category == selected;
                                  }).toList();
                                }
                              });
                            },
                            color: selected ==
                                    categoryLst.indexWhere(
                                        (element) => element.contains(e))
                                ? typeBtnColor
                                : Colors.white,
                            child: Text(
                              e.toString(),
                              style: TextStyle(
                                  fontSize: 12,
                                  color: selected ==
                                          categoryLst.indexWhere(
                                              (element) => element.contains(e))
                                      ? textColor
                                      : Colors.black87),
                            ));
                      }).toList(),
                    ),
                    Divider(
                      color: Theme.of(context).primaryColor,
                    ),
                    prodList
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class WarantyBanner extends StatelessWidget {
  const WarantyBanner({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 55.0,
      decoration: BoxDecoration(
        color: const Color(0xffffffff),
        border: Border.all(width: 2.0, color: const Color(0xffe2e2e2)),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            child: Row(
              children: <Widget>[
                SvgPicture.asset(
                  "assets/images/Mask Group 4.svg",
                  color: Theme.of(context).primaryColor,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'รวดเร็ว',
                      style: TextStyle(
                        fontFamily: 'Prompt',
                        fontSize: 8,
                        color: const Color(0xff646464),
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.left,
                    ),
                    Text(
                      'ถามปุ๊บตอบปั๊บ',
                      style: TextStyle(
                        fontFamily: 'Prompt',
                        fontSize: 7,
                        color: const Color(0xff646464),
                      ),
                      textAlign: TextAlign.left,
                    )
                  ],
                )
              ],
            ),
          ),
          Container(
            child: Row(
              children: <Widget>[
                SvgPicture.asset(
                  "assets/images/Mask Group 1.svg",
                  color: Theme.of(context).primaryColor,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'ปลอดภัย 100 %',
                      style: TextStyle(
                        fontFamily: 'Prompt',
                        fontSize: 8,
                        color: const Color(0xff646464),
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.left,
                    ),
                    Text(
                      'คุยได้ทุกเรื่องยกเว้นยืมตัง',
                      style: TextStyle(
                        fontFamily: 'Prompt',
                        fontSize: 7,
                        color: const Color(0xff646464),
                      ),
                      textAlign: TextAlign.left,
                    )
                  ],
                )
              ],
            ),
          ),
          Container(
            child: Row(
              children: <Widget>[
                SvgPicture.asset(
                  "assets/images/Mask Group 2.svg",
                  color: Theme.of(context).primaryColor,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'ใช้งานได้ 24 ชม.',
                      style: TextStyle(
                        fontFamily: 'Prompt',
                        fontSize: 8,
                        color: const Color(0xff646464),
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.left,
                    ),
                    Text(
                      'คุยได้ 24 ชม. แม้คนทำจะนอนแล้ว',
                      style: TextStyle(
                        fontFamily: 'Prompt',
                        fontSize: 7,
                        color: const Color(0xff646464),
                      ),
                      textAlign: TextAlign.left,
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
