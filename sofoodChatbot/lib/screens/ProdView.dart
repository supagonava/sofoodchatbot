import 'package:chatbot/components/EndDrawer.dart';
import 'package:chatbot/components/FloatBtn.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../models/Cart.dart';
import 'package:chatbot/models/Account.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import '../models/Account.dart';
import '../models/Product.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import '../models/Vote.dart';
import '../appConfig.dart';
import '../components/Waiting.dart';
import 'package:photo_view/photo_view.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'Cartpage.dart';
import 'LoginPage.dart';

class ProdView extends StatefulWidget {
  final Product product;

  const ProdView({Key key, @required this.product}) : super(key: key);

  @override
  _ProdViewState createState() => _ProdViewState();
}

class _ProdViewState extends State<ProdView> {
  int unitInput = 1;
  int indexForStack = 0, countVote = 0;
  List voteLst;
  int serviceType = 1;

  @override
  void initState() {
    Map data = {
      "user_id": Account.id,
      "action": "view",
      "product_id": widget.product.id
    };
    http
        .post("http://202.183.198.250/admin/myadmin/web/site/action-log",
            headers: AppConfig.header, body: convert.jsonEncode(data))
        .then((value) => print(value.body));
    print(widget.product.id);
    super.initState();
    Vote.getVote(widget.product.id).then((value) {
      setState(() {
        voteLst = value;
        countVote = voteLst.length;
        // ignore: unnecessary_statements
        indexForStack = 1;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    var imageView = Container(
      color: Colors.white,
      width: width,
      height: 270,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          GestureDetector(
            onTap: () => Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) =>
                    HeroView(subImage: widget.product.image))),
            child: Container(
              child: Image.network(
                widget.product.image,
                height: 150,
              ),
            ),
          ),
        ],
      ),
    );
    var description = Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Text(
            'รายละเอียด',
            style: TextStyle(
              fontFamily: 'Prompt',
              fontSize: 24,
              color: const Color(0xff646464),
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.left,
          ),
          Container(
            width: width * 1,
            height: 284.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(11.0),
              color: const Color(0xffffffff),
              border: Border.all(width: 1.0, color: const Color(0xffe1e1e1)),
            ),
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: SingleChildScrollView(
                child: Html(
                  data: "${widget.product.description}",
                ),
              ),
            ),
          )
        ],
      ),
    );
    var inputUnit = Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Container(
              width: width * 0.47,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'ราคา:',
                    style: TextStyle(
                      fontFamily: 'Prompt',
                      fontSize: 16,
                      color: const Color(0xff646464),
                    ),
                    textAlign: TextAlign.left,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    (() {
                      return '${AppConfig.formatCurrency.format(widget.product.price)} บาท';
                    })(),
                    style: TextStyle(
                      fontFamily: 'Prompt',
                      fontSize: 16,
                      color: const Color(0xffff8e0a),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.fade,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  serviceType == 1
                      ? Wrap(
                          direction: Axis.horizontal,
                          children: [
                            Text(
                              'ระบุจำนวน :',
                              style: TextStyle(
                                fontFamily: 'Prompt',
                                fontSize: 16,
                                color: const Color(0xff646464),
                              ),
                              textAlign: TextAlign.left,
                            ),
                            Container(
                              padding: EdgeInsets.only(right: 10),
                              height: 50,
                              child: TextField(
                                onChanged: (value) {
                                  setState(() {
                                    unitInput = int.parse(value);
                                  });
                                },
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(12)),
                                    labelText: "จำนวน $unitInput"),
                              ),
                            )
                          ],
                        )
                      : SizedBox()
                ],
              )),
          Container(
              width: width * 0.47,
              height: 210,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25.0),
                color: const Color(0xffffffff),
                border: Border.all(width: 2.0, color: const Color(0xffe2e2e2)),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(
                    () {
                      return '${AppConfig.formatCurrency.format(widget.product.price * unitInput)} ฿';
                    }(),
                    style: TextStyle(
                      fontFamily: 'Prompt',
                      fontSize: 30,
                      color: const Color(0xff303030),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  ButtonTheme(
                    minWidth: width * 0.4,
                    height: 50,
                    child: FlatButton(
                        color: Theme.of(context).primaryColor,
                        splashColor: Colors.greenAccent,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        onPressed: () async {
                          Cart.pushCart(widget.product.id.toString(),
                                  unitInput.toString())
                              .then((value) {
                            if (value) {
                              Fluttertoast.showToast(msg: "หยิบใส่ตะกร้าแล้ว");
                              Navigator.of(context).pop();
                            } else {
                              Fluttertoast.showToast(
                                  msg: "หยิบใส่ตะกร้าไม่สำเร็จ");
                            }
                          });
                        },
                        child: Icon(Icons.add_shopping_cart)),
                  )
                ],
              ))
        ],
      ),
    );
    var reviewBox = Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'รีวิวจากผู้ซื้อ ($countVote)',
            style: TextStyle(
              fontFamily: 'Prompt',
              fontSize: 16,
              color: const Color(0xff646464),
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.left,
          ),
          Container(
            width: width * 0.95,
            height: countVote > 0 ? 250 : 100,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(11.0),
              color: const Color(0xffffffff),
              border: Border.all(width: 1.0, color: const Color(0xffe1e1e1)),
            ),
            child: countVote > 0
                ? SingleChildScrollView(
                    child: Column(
                        children: voteLst
                            .map((e) => Container(
                                  margin: EdgeInsets.only(bottom: 1),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.black38, width: 0.5),
                                  ),
                                  child: ListTile(
                                    leading: Icon(Icons.account_circle,
                                        color: Colors.black54, size: 30),
                                    title: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(e.reviewerName),
                                        Wrap(
                                          direction: Axis.horizontal,
                                          children: List.generate(
                                            e.score,
                                            (index) => Icon(
                                              Icons.star,
                                              color: Color(0xffF8DA48),
                                              size: 16,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    subtitle: Text(
                                      e.reviewTxt,
                                      style: TextStyle(fontSize: 12),
                                    ),
                                  ),
                                ))
                            .toList()),
                  )
                : Center(
                    child: Text(
                      "ยังไม่มีรีวิว",
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
          )
        ],
      ),
    );
    return IndexedStack(
      index: indexForStack,
      children: [
        Waiting(),
        Scaffold(
          floatingActionButton: FloatBtn(),
          endDrawer: EndDrawer(),
          appBar: AppBar(
            title: Wrap(
              direction: Axis.horizontal,
              children: [
                Container(
                  width: width * 0.5,
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    widget.product.name,
                    style: TextStyle(fontSize: 16),
                    textAlign: TextAlign.center,
                  ),
                ),
                ButtonTheme(
                  minWidth: width * 0.1,
                  child: FlatButton(
                    child: Icon(Icons.shopping_cart),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) =>
                              Account.username != null &&
                                      AppConfig.token != null
                                  ? MyCart()
                                  : LoginPage()));
                    },
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                )
              ],
            ),
          ),
          body: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Material(
              child: KeyboardAvoider(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 10,
                      ),
                      imageView,
                      SizedBox(
                        height: 8,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Wrap(
                          children: List.generate(
                            widget.product.score,
                            (index) => Icon(
                              Icons.star,
                              color: Color(0xffF8DA48),
                              size: 20,
                            ),
                          ),
                          direction: Axis.horizontal,
                          spacing: 3,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Text(
                          '${widget.product.name}',
                          style: TextStyle(
                            fontFamily: 'Prompt',
                            fontSize: 18,
                            color: const Color(0xff646464),
                            fontWeight: FontWeight.w700,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Text(
                          'มีสินค้า',
                          style: TextStyle(
                            fontFamily: 'Prompt',
                            fontSize: 16,
                            color: const Color(0xff59cf1f),
                            fontWeight: FontWeight.w500,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      AppConfig.token != null && Account.id != null
                          ? inputUnit
                          : SizedBox(
                              height: 1,
                            ),
                      description,
                      reviewBox,
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ButtonTheme(
                          minWidth: width * 0.95,
                          height: 50,
                          child: FlatButton(
                            color: Theme.of(context).primaryColor,
                            splashColor: Colors.greenAccent,
                            onPressed: () {
                              Account.id != null
                                  // ignore: unnecessary_statements
                                  ? null
                                  : Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (BuildContext context) {
                                      return LoginPage();
                                    }));
                            },
                            child: Text("รีวิวสินค้านี้ +",
                                style: Theme.of(context).textTheme.bodyText2),
                          ),
                        ),
                        // ignore: dead_code
                      ),
                      SizedBox(
                        height: 80,
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class HeroView extends StatelessWidget {
  final String subImage;
  const HeroView({@required this.subImage});

  @override
  Widget build(BuildContext context) {
    PhotoViewHeroAttributes heroAttributes =
        PhotoViewHeroAttributes(tag: "subimage-view");
    return GestureDetector(
      onTap: () => Navigator.of(context).pop(),
      child: Scaffold(
        body: Center(
          child: PhotoView(
            imageProvider: NetworkImage(subImage),
            heroAttributes: heroAttributes,
          ),
          // child: Hero(
          //   tag: 'subimage-view',
          //   child: Image.network(subImage),
          // ),
        ),
      ),
    );
  }
}
