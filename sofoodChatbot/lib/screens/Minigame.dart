import 'package:chatbot/models/Account.dart';
import 'package:chatbot/models/Product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:math';
import '../appConfig.dart';
import '../components/Waiting.dart';
import 'Chatpage.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class MiniGame extends StatefulWidget {
  MiniGame({Key key}) : super(key: key);

  createState() => MiniGameState();
}

class ImageNameProd {
  final String image, name;
  ImageNameProd(this.image, this.name);
}

class MiniGameState extends State<MiniGame> {
  final _random = new Random();
  List<ImageNameProd> myChoise = [];
  int indexStack = 0;
  int time = 30;
  bool isFinished = false;
  List<int> randomNumlist = [];
  @override
  void initState() {
    Map data = {"user_id": Account.id, "action": "view", "product_id": null};
    http.post("http://202.183.198.250/admin/myadmin/web/site/action-log",
        headers: AppConfig.header, body: convert.jsonEncode(data));

    print("minigame initstate");
    super.initState();
    while (myChoise.length < 5) {
      int randomNum = _random.nextInt(Product.prodLst.length - 1);
      if (randomNumlist.where((element) => element == randomNum).length <= 0) {
        randomNumlist.insert(0, randomNum);
        myChoise.insert(
            0,
            ImageNameProd(Product.prodLst[randomNum].image,
                Product.prodLst[randomNum].name));
      }
    }
    setState(() {
      indexStack = 1;
    });
    countDown();
  }

  Future<void> countDown() async {
    setState(() {
      time--;
    });
    if (time <= 0) {
      isFinished = true;
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setBool("${Account.id}playedGame", true);

      Fluttertoast.showToast(msg: "หมดเวลา");
      showCupertinoDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("หมดเวลา!"),
              actions: [
                RaisedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                    // Navigator.of(context).pushAndRemoveUntil(
                    //     MaterialPageRoute(
                    //         builder: (BuildContext context) => HomePage()),
                    //     (route) => false);
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (BuildContext context) => ChatPage()));
                  },
                  child: Text("ตกลง"),
                )
              ],
            );
          });
    }
    if (!isFinished) {
      Future.delayed(Duration(seconds: 1)).then((value) => countDown());
    }
  }

  onfinish() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool("${Account.id}playedGame", true);
    Fluttertoast.showToast(msg: "คุณชนะแล้ว");
    setState(() {
      isFinished = true;
      Account.point += 150;
    });
    await Account.updatePoint();
    showCupertinoDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Column(
              children: [
                Text("นายท่านทำสำเร็จเจ้าค่ะ!"),
                Text(
                  "หนูเพิ่มโบนัสให้อีก 150 แต้มนะเจ้าคะ",
                  style: TextStyle(fontSize: 14),
                )
              ],
            ),
            actions: [
              RaisedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();
                  // Navigator.of(context).pushAndRemoveUntil(
                  //     MaterialPageRoute(
                  //         builder: (BuildContext context) => HomePage()),
                  //     (route) => false);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => ChatPage()));
                },
                child: Text("ตกลง"),
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return IndexedStack(
      index: indexStack,
      children: [
        Waiting(),
        SingleChildScrollView(
          child: Material(
            child: Column(
              children: [
                Container(
                  height: 100,
                  color: Theme.of(context).primaryColor,
                  child: Center(
                    child: Text(
                      "เกมส์ลากจับคู่ล่าแต้ม\nนับเวลาถอยหลัง\nต้องจับคู่ภายใน $time วินาที",
                      style: TextStyle(color: Colors.black, fontSize: 18),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: myChoise.map((element) {
                          return Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Draggable(
                              data: myChoise.indexWhere(
                                  (newEle) => newEle.name == element.name),
                              child: Container(
                                child: Image.network(
                                  element.image,
                                  height: 100,
                                  fit: BoxFit.contain,
                                  alignment: Alignment.center,
                                ),
                              ),
                              feedback: Container(
                                child: Image.network(
                                  element.image,
                                  height: 100,
                                  fit: BoxFit.contain,
                                  alignment: Alignment.center,
                                ),
                              ),
                              childWhenDragging: Container(),
                            ),
                          );
                        }).toList()),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: myChoise
                          .map((element) => Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: DragTarget(
                                  builder: (BuildContext context,
                                      List<int> candidateData,
                                      List<dynamic> rejectedData) {
                                    return Container(
                                      width: 120,
                                      height: 100,
                                      decoration: BoxDecoration(
                                          color: Colors.amber[300],
                                          borderRadius:
                                              BorderRadius.circular(25),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Colors.black87,
                                                offset:
                                                    Offset.fromDirection(1, 3),
                                                blurRadius: 0.5)
                                          ]),
                                      child: Center(
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            element.name,
                                            style: TextStyle(
                                              color: Colors.black,
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                  onWillAccept: (int data) {
                                    print("will Acceopt");

                                    return data ==
                                        myChoise.indexWhere((newEle) =>
                                            newEle.name ==
                                            element
                                                .name); // accept when data = 1 only.
                                  },
                                  onAccept: (int data) async {
                                    Fluttertoast.showToast(
                                        msg: "+50 แต้มเจ้าค่ะ");
                                    setState(() {
                                      Account.point += 50;
                                    });
                                    Account.updatePoint();
                                    if (myChoise.length <= 1) {
                                      isFinished = true;
                                      await onfinish();
                                    }
                                    setState(() {
                                      myChoise.removeAt(data);
                                    });

                                    print("Acceopted");
                                  },
                                ),
                              ))
                          .toList()
                            ..shuffle(Random(0)),
                    )
                  ],
                ),
                Container(
                  height: 100,
                  color: Theme.of(context).primaryColor,
                  child: Center(
                    child: Text(
                      "แต้มของคุณ ${Account.point}",
                      style: TextStyle(color: Colors.black, fontSize: 25),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
