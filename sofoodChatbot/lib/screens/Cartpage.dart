import 'package:chatbot/components/FloatBtn.dart';
import 'package:chatbot/models/Account.dart';
import 'package:chatbot/models/Order.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/Cart.dart';
import '../components/Waiting.dart';

class MyCart extends StatefulWidget {
  @override
  _MyCartState createState() => _MyCartState();
}

class _MyCartState extends State<MyCart> {
  int index = 0;
  int sumPrice = 0;

  @override
  void initState() {
    Cart.getCart().then((value) {
      Cart.cartList.forEach((element) {
        sumPrice += element.quantity * element.prod.price;
      });
      setState(() {
        index = 1;
      });
    });
    super.initState();
  }

  
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      floatingActionButton: FloatBtn(),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: width * 0.49,
              height: 80,
              child: FlatButton(
                color: Theme.of(context).primaryColor,
                child: Text("ทำการสั่งซื้อเดี๋ยวนี้ ($sumPrice บาท)"),
                onPressed: () async {
                  await Order.makeOrder();
                  Navigator.of(context).pop();
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => MyCart()));
                },
              ),
            ),
            Container(
              width: width * 0.5,
              height: 80,
              child: FlatButton(
                color: Colors.red,
                child: Text("เคลียร์ตะกร้า"),
                onPressed: () async {
                  setState(() {
                    index = 0;
                  });
                  await Cart.removeCart(Account.id);
                  Navigator.of(context).pop();
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => MyCart()));
                },
              ),
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: Row(
          children: [
            Icon(Icons.shopping_cart),
            Text("ตะกร้าสินค้า"),
          ],
        ),
      ),
      body: IndexedStack(
        index: index,
        children: [
          Waiting(),
          SingleChildScrollView(
            child: Column(
              children: [
                Column(
                  children: Cart.cartList
                      .map((e) => Container(
                            height: 120,
                            width: width,
                            margin: EdgeInsets.fromLTRB(0, 0, 0, 8),
                            decoration: BoxDecoration(
                                border: Border.all(
                                    width: 0.5,
                                    color: Theme.of(context).primaryColor),
                                borderRadius: BorderRadius.circular(12)),
                            child: ListTile(
                              title: Wrap(
                                direction: Axis.horizontal,
                                children: [
                                  Image.network(
                                    e.prod.image,
                                    width: width * 0.2,
                                    height: 110,
                                    fit: BoxFit.cover,
                                  ),
                                  Container(
                                    width: width * 0.70,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "สินค้า ${e.prod.name}",
                                            style: TextStyle(fontSize: 16),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                          Text("ราคา ${e.prod.price} บาท",
                                              style: TextStyle(fontSize: 14),
                                              overflow: TextOverflow.ellipsis),
                                          Text("จำนวน ${e.quantity} ชิ้น",
                                              style: TextStyle(fontSize: 12),
                                              overflow: TextOverflow.ellipsis)
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ))
                      .toList(),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
