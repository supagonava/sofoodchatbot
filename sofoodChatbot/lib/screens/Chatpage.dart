import 'dart:math';
import 'package:chatbot/components/EndDrawer.dart';
import 'package:chatbot/components/GiftProduct.dart';
import 'package:chatbot/components/PromotionProduct.dart';
import 'package:chatbot/components/Waiting.dart';
import 'package:chatbot/models/Account.dart';
import 'package:chatbot/models/Cart.dart';
import 'package:chatbot/models/ChatMessage.dart';
import 'package:chatbot/models/Order.dart';
import 'package:chatbot/models/Product.dart';
import 'package:chatbot/screens/LoginPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dialogflow/dialogflow_v2.dart';
import 'package:flutter_text_to_speech/flutter_text_to_speech.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:speech_to_text/speech_recognition_error.dart';
import 'package:speech_to_text/speech_recognition_result.dart';
import 'package:speech_to_text/speech_to_text.dart';

import '../appConfig.dart';
import 'Minigame.dart';
import 'ProdView.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class ChatPage extends StatefulWidget {
  ChatPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ChatPage createState() => _ChatPage();
}

class _ChatPage extends State<ChatPage> {
  bool todayLogin = false;
  VoiceController _voiceController;
  bool isSpeaker = true, playedGame = false;
  String text = 'ขอโทษค่ะฉันไม่เข้าใจ';
  int indexStack = 0;

  Future<void> isTodayLogin() async {
    if (Account.id != null) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      if (prefs.getBool("${Account.id}TodayLogin") != null &&
          prefs.getBool("${Account.id}TodayLogin")) {
        todayLogin = true;
      }
      print(todayLogin);
    }
  }

  @override
  void initState() {
    initSpeechState();
    initDiaryLogin();

    _voiceController = FlutterTextToSpeech.instance.voiceController();
    var message = ChatMessage(
      text: "น้องจูเลียตสวัสดีค่ะ นายท่านต้องการให้น้องจูเลียตทำอะไรคะ",
      name: "น้องจูเลียต",
      type: false,
      widget: null,
    );
    setState(() {
      ChatMessage.chatLst.insert(0, message);
      _playVoice("น้องจูเลียตสวัสดีค่ะ นายท่านต้องการให้น้องจูเลียตทำอะไรคะ");
    });
    isTodayLogin().then((_) async {
      if (Account.id != null) {
        await ChatMessage.getOnlineMessage();
      }
      if (Account.id != null && !todayLogin) {
        Account.point += 150;
        Account.updatePoint();
        showCupertinoDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: Column(
                  children: [
                    Text("นายท่านกลับมาแล้ว!"),
                    Text(
                        "สวัสดีค่ะนายท่าน ${Account.username}\nยินดีต้อนรับนะคะ + 150 แต้ม",
                        style: TextStyle(fontSize: 14)),
                  ],
                ),
                actions: [
                  FlatButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: Text("ตกลง"))
                ],
              );
            });
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setBool("${Account.id}TodayLogin", true);
      }
      setState(() {
        indexStack = 1;
      });
    });
    Account.checkLogin().then((value) => indexStack = 1);
    super.initState();
  }

  initDiaryLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // prefs.clear();
    if (prefs.getBool("${Account.id}playedGame") != null &&
        prefs.getBool("${Account.id}playedGame") == true) {
      setState(() {
        playedGame = true;
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    _voiceController.stop();
  }

  _playVoice(mytext) {
    _voiceController.init().then((_) {
      _voiceController.speak(
        mytext,
        VoiceControllerOptions(speechRate: 1.6, pitch: 1),
      );
    });
  }

  bool _hasSpeech = false;
  double level = 0.0;
  double minSoundLevel = 50000;
  double maxSoundLevel = -50000;
  String lastWords = "";
  String lastError = "";
  String lastStatus = "";
  String _currentLocaleId = "";
  final SpeechToText speech = SpeechToText();

  Future<void> initSpeechState() async {
    bool hasSpeech = await speech.initialize(
        onError: errorListener, onStatus: statusListener);
    if (hasSpeech) {
      var systemLocale = await speech.systemLocale();
      _currentLocaleId = systemLocale.localeId;
    }

    if (!mounted) return;

    setState(() {
      _hasSpeech = hasSpeech;
    });
  }

  final TextEditingController _textController = TextEditingController();
  String queryTxt;
  Widget _buildTextComposer() {
    return IconTheme(
      data: IconThemeData(color: Theme.of(context).accentColor),
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 8.0),
        color: Colors.white70,
        height: 60,
        child: Row(
          children: <Widget>[
            ButtonTheme(
              minWidth: 20,
              child: FlatButton(
                onPressed: () {
                  if (!_hasSpeech) {
                    initSpeechState();
                    // print("no listening");
                  } else if (speech.isListening) {
                    // print("stop listening");
                    stopListening();
                  } else {
                    // print("start listening");
                    Fluttertoast.showToast(msg: "พูดได้เลย!");
                    startListening();
                  }
                },
                child: Icon(
                  Icons.mic,
                  size: 35,
                  color: !speech.isListening
                      ? Color.fromRGBO(0, 0, 0, 1)
                      : Theme.of(context).primaryColor,
                ),
              ),
            ),
            Flexible(
              child: TextField(
                style: TextStyle(fontSize: 16),
                controller: _textController,
                scrollPadding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                onSubmitted: _handleSubmitted,
                decoration: InputDecoration(
                    hintText: "พิมพ์ข้อความ",
                    hintStyle: TextStyle(fontSize: 16),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30)),
                    contentPadding: const EdgeInsets.fromLTRB(15, 10, 15, 10)),

                /*
                 padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: Color.fromRGBO(255, 206, 41, 0.7),
                 */
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 4.0),
              child: IconButton(
                  icon: Icon(
                    Icons.send,
                    size: 35,
                    color: Color.fromRGBO(255, 206, 41, 1),
                  ),
                  onPressed: () => _handleSubmitted(_textController.text)),
            ),
          ],
        ),
      ),
    );
  }

  // ignore: non_constant_identifier_names
  void ResponseQuery(query) async {
    AuthGoogle authGoogle =
        await AuthGoogle(fileJson: "assets/myDialogFlow.json").build();
    Dialogflow dialogflow =
        Dialogflow(authGoogle: authGoogle, language: Language.english);
    AIResponse response = await dialogflow.detectIntent(query);
    var res = response.getListMessage();
    // print(res);
    // print(res.length);

    String action = 'basic';

    if (res.length > 1) {
      action = res[1]['payload']['action'];
    }
    print("action = " + action);
    ChatMessage message;
    if (action == "exchangePoint") {
      message = ChatMessage(
        name: "น้องจูเลียต",
        text: response.getMessage(),
        type: false,
        widget: null,
      );

      showDialog(
          context: context,
          builder: (BuildContext ctx) {
            return Dialog(child: GiftProduct());
          });
    } else if (action == "playgame") {
      message = ChatMessage(
        name: "น้องจูเลียต",
        text: response.getMessage(),
        type: false,
        widget: null,
      );
      showGameDialog(context);
    } else if (action.contains('promotion')) {
      showDialog(
          context: context,
          builder: (BuildContext ctx) {
            return Dialog(child: PromotionProduct());
          });
      final List<String> bannerLst = [
        'https://i.ibb.co/tpwbw6t/115824997-287740619162014-3822775994726599775-n.png',
        'https://i.ibb.co/ngYQ8rQ/Sofood.png',
      ];
      message = ChatMessage(
        text: response.getMessage() ??
            CardDialogflow(response.getListMessage()[0]).title,
        name: "น้องจูเลียต",
        type: false,
        widget: Container(
          height: 410,
          child: Column(
            children: [
              FlatButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => PromotionProduct()));
                },
                child: Image.network(
                  bannerLst[1],
                  height: 200,
                ),
              ),
              InkWell(
                onTap: () => _handleSubmitted("แต้มของฉัน"),
                child: Image.network(
                  bannerLst[0],
                  height: 200,
                ),
              ),
            ],
          ),
        ),
      );
    } else if (action == "gethistory") {
      message = ChatMessage(
        text: response.getMessage() ??
            CardDialogflow(response.getListMessage()[0]).title,
        name: "น้องจูเลียต",
        type: false,
        widget: null,
      );
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return OrderDialog();
          });
    } else if (action == "getpoint") {
      if (Account.id != null) {
        message = ChatMessage(
          name: "น้องจูเลียต",
          text: "แต้มของนายท่านคือ ${Account.point}",
          type: false,
          widget: null,
        );
      } else {
        text = "โอ๊ะโอ่ ดูเหมือนว่านายท่านจะยังไม่ได้เข้าสู่ระบบนะคะ";
        message = ChatMessage(
          name: "น้องจูเลียต",
          text: "โอ๊ะโอ่ ดูเหมือนว่านายท่านจะยังไม่ได้เข้าสู่ระบบนะคะ",
          type: false,
          widget: Container(
            height: 40,
            child: FlatButton(
                color: Colors.white,
                onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => LoginPage())),
                child: Text(
                  "ไปหน้าเข้าสู่ระบบ",
                  style: TextStyle(color: Colors.black),
                )),
          ),
        );
      }
    } else if (action == "search") {
      String keyword = res[1]['payload']['product'];
      Product.prodLst = Product.duplicateList
          .where((element) =>
              element.name.contains(keyword) ||
              element.description.contains(keyword))
          .toList();
      message = ChatMessage(
        text: response.getMessage() ??
            CardDialogflow(response.getListMessage()[0]).title,
        name: "น้องจูเลียต",
        type: false,
        widget: Product.prodLst.length > 0
            ? Container(
                height: 180,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: Product.prodLst
                      .map((e) => Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: FlatButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25)),
                                splashColor: Theme.of(context).primaryColor,
                                color: Colors.transparent,
                                onPressed: () => Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            ProdView(
                                              product: e,
                                            ))),
                                child: Column(
                                  children: [
                                    Image.network(
                                      e.image,
                                      fit: BoxFit.contain,
                                      height: 140,
                                    ),
                                    Text(
                                      e.name,
                                      style: TextStyle(
                                          color: Colors.black87, fontSize: 14),
                                    )
                                  ],
                                )),
                          ))
                      .toList(),
                ))
            : Text(
                "ไม่พบสินค้าใดที่ตรงกับ $keyword",
                style: TextStyle(color: Colors.red),
              ),
      );
    } else if (action.contains("getcart")) {
      if (Account.id != null) {
        await Cart.getCart();
        message = ChatMessage(
          text: response.getMessage() ??
              CardDialogflow(response.getListMessage()[0]).title,
          name: "น้องจูเลียต",
          type: false,
          widget: Cart.cartList != null && Cart.cartList.length > 0
              ? Container(
                  height: 250,
                  child: Column(
                    children: [
                      Container(
                        height: 200,
                        child: ListView(
                            scrollDirection: Axis.vertical,
                            children: Cart.cartList
                                .map((e) => Padding(
                                      padding: const EdgeInsets.all(3.0),
                                      child: Container(
                                        color: Colors.deepOrange,
                                        child: ListTile(
                                          leading: Text(
                                            e.prod.name,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 10),
                                          ),
                                          trailing: Text(
                                            " ${e.prod.price} บาท ${e.quantity} ชิ้น",
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 8),
                                          ),
                                        ),
                                      ),
                                    ))
                                .toList()),
                      ),
                      Row(
                        children: [
                          FlatButton.icon(
                              color: Colors.white,
                              onPressed: () async {
                                await Order.makeOrder();
                                setState(() {
                                  ChatMessage.chatLst.insert(
                                      0,
                                      ChatMessage(
                                        name: "น้องจูเลียต",
                                        text:
                                            "น้องจูเลียตสั่งสินค้าให้แล้วนะคะ",
                                        type: false,
                                        widget: null,
                                      ));
                                });
                                _playVoice("น้องจูเลียตสั่งสินค้าให้แล้วนะคะ");
                              },
                              icon: Icon(Icons.fastfood),
                              label: Text("สั่งซื้อสินค้า")),
                        ],
                      )
                    ],
                  ),
                )
              : Text(
                  "ไม่มีสินค้าในตะกร้าเลยค่ะ",
                  style: TextStyle(color: Colors.red),
                ),
        );
      } else {
        text = "โอ๊ะโอ่ ดูเหมือนว่านายท่านจะยังไม่ได้เข้าสู่ระบบนะคะ";
        message = ChatMessage(
          name: "น้องจูเลียต",
          text: "โอ๊ะโอ่ ดูเหมือนว่านายท่านจะยังไม่ได้เข้าสู่ระบบนะคะ",
          type: false,
          widget: Container(
            height: 40,
            child: FlatButton(
                color: Colors.white,
                onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => LoginPage())),
                child: Text(
                  "ไปหน้าเข้าสู่ระบบ",
                  style: TextStyle(color: Colors.black),
                )),
          ),
        );
      }
    } else {
      message = ChatMessage(
        text: response.getMessage() ??
            CardDialogflow(response.getListMessage()[0]).title,
        name: "น้องจูเลียต",
        type: false,
        widget: null,
      );
    }

    ChatMessage.chatLst.insert(0, message);
    setState(() {
      // ignore: unnecessary_statements
      isSpeaker ? _playVoice(response.getMessage()) : null;
    });

    Map data = {
      "user_id": Account.id,
      "chat_message": query,
      "intent": action,
      "chat_response": response.getMessage()
    };
    http
        .post("http://202.183.198.250/admin/myadmin/web/site/chat-log",
            headers: AppConfig.header, body: convert.jsonEncode(data))
        .then((value) => print(value.body));
  }

  void _handleSubmitted(String text) {
    print("text=>" + text);
    if (text != '' && text != null) {
      _textController.clear();
      ChatMessage message = ChatMessage(
        text: text,
        name: "คุณ",
        type: true,
      );
      setState(() {
        ChatMessage.chatLst.insert(0, message);
      });
      ResponseQuery(text);
    } else {
      Fluttertoast.showToast(msg: "ใส่ข้อความก่อนนะคะ");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 206, 41, 1),
        centerTitle: true,
        actions: [
          FlatButton.icon(
              onPressed: () {
                setState(() {
                  isSpeaker = !isSpeaker;
                });
              },
              icon: Icon(() {
                if (isSpeaker)
                  return Icons.volume_down;
                else
                  return Icons.volume_mute;
              }()),
              label: Text(isSpeaker ? "ปิดเสียง" : "เปิดเสียง"))
        ],
        title: Text("น้องจูเลียต",
            style: TextStyle(
              color: Color.fromRGBO(64, 64, 64, 1),
              fontWeight: FontWeight.bold,
              fontSize: 16,
            )),
      ),
      body: IndexedStack(
        index: indexStack,
        children: [
          Waiting(),
          Column(children: <Widget>[
            Flexible(
                child: ListView.builder(
              padding: EdgeInsets.all(8.0),
              reverse: true,
              itemBuilder: (_, int index) => ChatMessage.chatLst[index],
              itemCount: ChatMessage.chatLst.length,
            )),
            Divider(height: 1.0),
            Container(
                height: 60,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    Account.id != null && !playedGame
                        ? Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: InkWell(
                              onTap: () {
                                _handleSubmitted("เล่นเกมส์ล่าแต้ม");
                              },
                              child: Chip(
                                label: Text(
                                  "เล่นเกมส์สะสมแต้ม",
                                  style: TextStyle(fontSize: 16),
                                ),
                                avatar: CircleAvatar(
                                  child: Icon(Icons.gamepad),
                                ),
                              ),
                            ),
                          )
                        : SizedBox(),
                    Account.id != null
                        ? Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: InkWell(
                              onTap: () {
                                _handleSubmitted("ตะกร้าสินค้าของฉัน");
                              },
                              child: Chip(
                                label: Text(
                                  "ตะกร้าสินค้าของฉัน",
                                  style: TextStyle(fontSize: 16),
                                ),
                                avatar: CircleAvatar(
                                  child: Icon(FontAwesomeIcons.shoppingBag),
                                ),
                              ),
                            ),
                          )
                        : SizedBox(),
                    Account.id != null
                        ? Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: InkWell(
                              onTap: () {
                                _handleSubmitted("คำสั่งซื้อ");
                              },
                              child: Chip(
                                label: Text(
                                  "คำสั่งซื้อ",
                                  style: TextStyle(fontSize: 16),
                                ),
                                avatar: CircleAvatar(
                                  child: Icon(FontAwesomeIcons.history),
                                ),
                              ),
                            ),
                          )
                        : SizedBox(),
                    Account.id != null
                        ? Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: InkWell(
                              onTap: () {
                                _handleSubmitted("แลกแต้ม");

                                // showDialog(

                                //     context: context,

                                //     builder: (BuildContext ctx) {

                                //       return Dialog(child: GiftProduct());

                                //     });
                              },
                              child: Chip(
                                label: Text(
                                  "แลกแต้ม",
                                  style: TextStyle(fontSize: 16),
                                ),
                                avatar: CircleAvatar(
                                  child: Icon(FontAwesomeIcons.award),
                                ),
                              ),
                            ),
                          )
                        : SizedBox(),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: InkWell(
                        onLongPress: () async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();

                          prefs.setBool("${Account.id}TodayLogin", false);

                          setState(() {
                            playedGame = false;

                            todayLogin = false;
                          });
                        },
                        onTap: () {
                          _handleSubmitted("มีโปรโมชั่นอะไรบ้าง");
                        },
                        child: Chip(
                          label: Text(
                            "โปรโมชั่น",
                            style: TextStyle(fontSize: 16),
                          ),
                          avatar: CircleAvatar(
                            child: Icon(Icons.fastfood),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: InkWell(
                        onTap: () => showDialog(
                          context: context,
                          builder: (context) {
                            var bugReport = Dialog(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25),
                                  side: BorderSide(width: 0.5)),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(25),
                                child: Container(
                                  height: 400,
                                  child: SingleChildScrollView(
                                    child: Column(
                                      children: [
                                        Container(
                                          height: 40,
                                          width: double.infinity,
                                          color: Theme.of(context).primaryColor,
                                          child: Text(
                                            "รายงานปัญหา",
                                            style: TextStyle(fontSize: 18),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: TextField(
                                            decoration: InputDecoration(
                                                labelText: "กรอกชื่อ",
                                                helperText: "ชื่อของคุณ"),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: TextField(
                                            maxLines: 1,
                                            decoration: InputDecoration(
                                                labelText: "กรอกรายละเอียด",
                                                helperText: "ปัญหาที่พบ"),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: ButtonTheme(
                                            height: 50,
                                            minWidth: double.infinity,
                                            child: FlatButton.icon(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                onPressed: () {
                                                  Navigator.of(context).pop();

                                                  Fluttertoast.showToast(
                                                      msg: "Reported");
                                                },
                                                icon: Icon(Icons.bug_report),
                                                label: Text("ส่งแจ้งปัญหา")),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );

                            return bugReport;
                          },
                        ),
                        child: Chip(
                          label: Text(
                            "เเจ้งปัญหา",
                            style: TextStyle(fontSize: 16),
                          ),
                          avatar: CircleAvatar(
                            child: Icon(Icons.bug_report),
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
            Container(
              decoration: BoxDecoration(color: Theme.of(context).cardColor),
              child: _buildTextComposer(),
            ),
          ]),
        ],
      ),
    );
  }

  Future showGameDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("วิธีการเล่น"),
            content: Text(
                "1. ดูที่ภาพทางฝั่งซ้ายและชื่อสินค้าทางฝั่งขวา\n2. ลากรูปภาพไปใส่ชื่อให้ถูกต้อง\n3. นายท่านจะมีเวลาเล่น 30 วินาที เท่านั้นนะเจ้าคะ \n4. และข้อสุดท้ายนายท่านจะเล่นได้วันละครั้งเท่านั้นเจ้าค่ะ"),
            actions: [
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(context),
                  child: Text("Cancel")),
              !playedGame
                  ? FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop(context);
                        showCupertinoDialog(
                            context: context,
                            builder: ((BuildContext context) {
                              return Dialog(child: MiniGame());
                            }));
                      },
                      child: Text("Ok"))
                  : Text(
                      "เล่นไปแล้ว",
                      style: TextStyle(color: Colors.black),
                    ),
            ],
          );
        });
  }

  void startListening() {
    lastWords = "";
    lastError = "";
    speech.listen(
        onResult: resultListener,
        listenFor: Duration(seconds: 10),
        localeId: _currentLocaleId,
        onSoundLevelChange: soundLevelListener,
        cancelOnError: true,
        partialResults: true,
        onDevice: true,
        listenMode: ListenMode.confirmation);
  }

  void stopListening() {
    speech.stop();
    setState(() {
      level = 0.0;
    });
  }

  void cancelListening() {
    speech.cancel();
    setState(() {
      level = 0.0;
    });
  }

  void resultListener(SpeechRecognitionResult result) {
    if (result.recognizedWords != '' &&
        result.recognizedWords != null &&
        result.finalResult != null &&
        result.finalResult) {
      _handleSubmitted(result.recognizedWords);
      print(result.recognizedWords);
    }
  }

  void soundLevelListener(double level) {
    minSoundLevel = min(minSoundLevel, level);
    maxSoundLevel = max(maxSoundLevel, level);
    // print("sound level $level: $minSoundLevel - $maxSoundLevel ");
    setState(() {
      this.level = level;
    });
  }

  void errorListener(SpeechRecognitionError error) {
    // print("Received error status: $error, listening: ${speech.isListening}");
    setState(() {
      lastError = "${error.errorMsg} - ${error.permanent}";
    });
  }

  void statusListener(String status) {
    // print(
    // "Received listener status: $status, listening: ${speech.isListening}");
    setState(() {
      lastStatus = "$status";
    });
  }
}
