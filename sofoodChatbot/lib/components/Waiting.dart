import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Waiting extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: AnimatedContainer(
        width: double.infinity,
        height: double.infinity,
        color: Colors.white,
        duration: Duration(seconds: 3),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SvgPicture.asset('assets/images/Loading.svg'),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CircularProgressIndicator(
                backgroundColor: Theme.of(context).primaryColor,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "อดใจรอสักครู่...",
                style: TextStyle(
                    fontSize: 16,
                    fontFamily: "Prompt",
                    fontWeight: FontWeight.normal,
                    color: Colors.black87),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
