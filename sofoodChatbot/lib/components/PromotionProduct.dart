import 'package:chatbot/appConfig.dart';
import 'package:chatbot/models/Account.dart';
import 'package:chatbot/screens/ProdView.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../models/Product.dart';
import 'Waiting.dart';
import 'package:http/http.dart' as http;

class PromotionProduct extends StatefulWidget {
  const PromotionProduct({
    Key key,
  }) : super(key: key);

  @override
  _PromotionProductState createState() => _PromotionProductState();
}

class _PromotionProductState extends State<PromotionProduct> {
  int indexStack = 0;
  List<Product> pointLst = [];

  @override
  void initState() {
    print('init');
    super.initState();
    pointLst.clear();
    pointLst = Product.duplicateList
        .where((element) => element.price >= 15 && element.price <= 25)
        .toList();
    pointLst.sort((a, b) => a.price.compareTo(b.price));
    setState(() {
      indexStack = 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("สินค้าลดราคา"),
        centerTitle: true,
      ),
      body: IndexedStack(
        index: indexStack,
        children: [
          Waiting(),
          SingleChildScrollView(
            child: Column(
              children: [
                Column(
                  children: pointLst
                      .map((e) => FlatButton(
                            onPressed: () =>
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (BuildContext context) => ProdView(
                                          product: e,
                                        ))),
                            child: Container(
                              margin: EdgeInsets.symmetric(vertical: 8),
                              decoration: BoxDecoration(
                                  border: Border.all(width: 0.5),
                                  borderRadius: BorderRadius.circular(10)),
                              child: ListTile(
                                leading: Image.network(e.image),
                                title: Container(
                                  width: 200,
                                  child: Text(
                                    e.name,
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                                subtitle: Text(
                                  "ลดเหลือ ${e.price}  บาท (ปกติ ${e.price + 10})",
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                                trailing: Text(
                                  "ช็อปเลย",
                                  style: TextStyle(color: Colors.green),
                                ),
                              ),
                            ),
                          ))
                      .toList(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
