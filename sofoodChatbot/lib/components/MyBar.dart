import 'package:chatbot/models/Account.dart';
import 'package:chatbot/screens/LoginPage.dart';
import 'package:flutter/material.dart';

import '../appConfig.dart';

class MyBar extends StatelessWidget {
  final width;

  const MyBar({Key key, @required this.width}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Wrap(
        direction: Axis.horizontal,
        children: [
          Image.asset(
            "assets/images/icon.png",
            width: width * 0.15,
          ),
          Container(
              margin: EdgeInsets.only(top: 5),
              width: width * 0.35,
              height: 45,
              child: TextField(
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  hintText: 'ค้นหา',
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20)),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                    borderRadius: BorderRadius.circular(25.7),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                    borderRadius: BorderRadius.circular(25.7),
                  ),
                ),
              )),
          ButtonTheme(
            minWidth: width * 0.1,
            child: FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)),
              onPressed: () {},
              child: Icon(Icons.search),
            ),
          ),
          ButtonTheme(
            minWidth: width * 0.1,
            child: FlatButton(
              child: Icon(Icons.shopping_cart),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) =>
                        Account.username != null && AppConfig.token != null
                            ? null
                            : LoginPage()));
              },
            ),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          )
        ],
      ),
    );
  }
}
