import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../appConfig.dart';
import '../screens/HomePage.dart';

class CustomBar extends StatefulWidget {
  @override
  _CustomBarState createState() => _CustomBarState();
}

class _CustomBarState extends State<CustomBar> {
  String searchTxt;
  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    var logoText = Text.rich(
      TextSpan(
        style: TextStyle(
          fontFamily: 'Roboto',
          fontSize: 36,
          color: const Color(0xff82c55b),
        ),
        children: [
          TextSpan(
            text: 'I',
            style: TextStyle(
              fontWeight: FontWeight.w900,
            ),
          ),
          TextSpan(
            text: 'AID',
            style: TextStyle(
              color: const Color(0xff303030),
              fontWeight: FontWeight.w900,
            ),
          ),
        ],
      ),
      textAlign: TextAlign.center,
    );

    return Container(
        width: width,
        height: 80,
        decoration: BoxDecoration(
          color: const Color(0xffffffff),
          boxShadow: [
            BoxShadow(
              color: const Color(0x29000000),
              offset: Offset(0, 3),
              blurRadius: 6,
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            GestureDetector(
              onTap: () => Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (BuildContext context) => HomePage()),
                  (route) => false),
              child: Container(
                child: logoText,
                width: width * 0.23,
              ),
            ),
            Row(
              children: <Widget>[
                Container(
                  width: width * 0.33,
                  height: 40.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        bottomLeft: Radius.circular(20)),
                    color: const Color(0xfff9f9f9),
                    border:
                        Border.all(width: 1.0, color: const Color(0xffe1e1e1)),
                  ),
                  child: Container(
                    height: 40,
                    padding: EdgeInsets.only(left: 10, bottom: 10),
                    child: TextField(
                      maxLines: 1,
                      decoration:
                          InputDecoration.collapsed(hintText: "ค้นหา..."),
                      onChanged: (value) => AppConfig.searchTxt = value,
                    ),
                  ),
                ),
                ButtonTheme(
                  minWidth: width * 0.07,
                  height: 40,
                  child: RaisedButton(
                    color: const Color(0xff82c55b),
                    splashColor: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomRight: Radius.circular(20))),
                    onPressed: () {
                      setState(() {
                        // Tool.toolList = Tool.duplicateList
                        //     .where((element) =>
                        //         element.descript
                        //             .contains("${AppConfig.searchTxt}") ||
                        //         element.name
                        //             .contains("${AppConfig.searchTxt}") ||
                        //         element.priceRate
                        //             .toString()
                        //             .contains("${AppConfig.searchTxt}") ||
                        //         element.sellPrice
                        //             .toString()
                        //             .contains("${AppConfig.searchTxt}"))
                        //     .toList();
                      });
                    },
                    child: Icon(
                      Icons.search,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
            ButtonTheme(
              minWidth: width * 0.05,
              child: FlatButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                color: Colors.transparent,
                splashColor: Theme.of(context).primaryColor,
                child: SvgPicture.asset(
                  "assets/images/blogging.svg",
                  color: Theme.of(context).primaryColor,
                ),
                onPressed: () {
                  // if (Account.id == null) {
                  //   Navigator.of(context).push(MaterialPageRoute(
                  //       builder: (BuildContext context) => LoginPage()));
                  // } else {
                  //   showDialog(
                  //       context: context,
                  //       builder: (BuildContext context) => MyOrderPage());
                  // }
                },
              ),
            ),
            ClipOval(
              child: Material(
                color: Theme.of(context).primaryColor, // button color
                child: InkWell(
                  splashColor: Theme.of(context).buttonColor, // inkwell color
                  child: SizedBox(
                      width: width * 0.08,
                      height: width * 0.08,
                      child: Icon(
                        Icons.account_circle,
                        color: Colors.white,
                      )),
                  onTap: () {
                    // if (Account.id == null) {
                    //   Navigator.of(context).push(MaterialPageRoute(
                    //       builder: (BuildContext context) =>
                    //           LoginPage()));
                    // } else {
                    //   showDialog(
                    //       context: context,
                    //       builder: (BuildContext context) => MyAccount());
                    // }
                  },
                ),
              ),
            ),
          ],
        ));
  }
}
