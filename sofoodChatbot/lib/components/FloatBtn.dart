import 'package:chatbot/screens/Chatpage.dart';
import 'package:flutter/material.dart';

class FloatBtn extends StatelessWidget {
  const FloatBtn({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      height: 50,
      child: FlatButton(
        color: Theme.of(context).primaryColor,
        onPressed: () {
          Navigator.of(context).push(
              MaterialPageRoute(builder: (BuildContext context) => ChatPage()));
        },
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
        child: Wrap(
          direction: Axis.horizontal,
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            Image.asset(
              "assets/images/icon.png",
              width: 50,
            ),
            Text(
              "คุยกับจูเลียต",
              style: TextStyle(color: Colors.black87),
            )
          ],
        ),
      ),
    );
  }
}
