import 'package:chatbot/appConfig.dart';
import 'package:chatbot/models/Account.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../models/Product.dart';
import 'Waiting.dart';
import 'package:http/http.dart' as http;

class GiftProduct extends StatefulWidget {
  const GiftProduct({
    Key key,
  }) : super(key: key);

  @override
  _GiftProductState createState() => _GiftProductState();
}

class _GiftProductState extends State<GiftProduct> {
  int indexStack = 0;
  List<Product> pointLst = [];

  @override
  void initState() {
    print('init');
    super.initState();
    pointLst.clear();
    pointLst = Product.duplicateList
        .where((element) => element.price >= 15 && element.price <= 25)
        .toList();
    pointLst.sort((a, b) => a.price.compareTo(b.price));
    setState(() {
      indexStack = 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height - 100,
      child: IndexedStack(
        index: indexStack,
        children: [
          Waiting(),
          SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: 100,
                  color: Theme.of(context).primaryColor,
                  child: Center(
                    child: Text(
                      "แลกแต้มอิ่มท้อง\nแต้มที่มี ${Account.point}",
                      style: TextStyle(color: Colors.black, fontSize: 18),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Column(
                  children: pointLst
                      .map((e) => FlatButton(
                            onPressed: () {
                              if (Account.point >= e.price * 15) {
                                showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      title: Text("ยืนยันใช้คะแนน"),
                                      content: Text(
                                          "นายท่านยืนยันที่จะใช้ ${e.price * 15} คะแนนหรือเปล่าคะ?"),
                                      actions: [
                                        RaisedButton(
                                          onPressed: () async {
                                            Account.point -= e.price * 15;
                                            await Account.updatePoint();
                                            setState(() {
                                              indexStack = 0;
                                            });
                                            await http
                                                .put(AppConfig.host, headers: {
                                              "command":
                                                  "INSERT INTO myorder (user_id,status,shipping_address) VALUES (${Account.id},1,(SELECT users.province FROM users WHERE users.id = ${Account.id}));"
                                            }).then((value) {
                                              var sql =
                                                  "INSERT INTO order_detail (order_id,product_id,quantity,price) VALUES ((SELECT id FROM myorder WHERE myorder.user_id = ${Account.id} ORDER BY id DESC LIMIT 1),${e.id},1,0);";
                                              http.put(
                                                  "http://202.183.198.250:5000",
                                                  headers: {
                                                    "command": sql
                                                  }).then((value) {
                                                setState(() {
                                                  indexStack = 1;
                                                });
                                                Fluttertoast.showToast(
                                                    msg:
                                                        "แลกสินค้าแล้ว เช็ครายการได้ที่ประวัติคำสั่งซื้อเจ้าค่ะ");
                                              });
                                            });

                                            Navigator.of(context).pop();
                                          },
                                          child: Text("Ok"),
                                        ),
                                        RaisedButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: Text("cancel"),
                                        )
                                      ],
                                    );
                                  },
                                );
                              } else {
                                Fluttertoast.showToast(
                                    msg: "แต้มของนายท่านไม่เพียงพอค่ะ");
                              }
                            },
                            child: Container(
                              margin: EdgeInsets.symmetric(vertical: 8),
                              decoration: BoxDecoration(
                                  border: Border.all(width: 0.5),
                                  borderRadius: BorderRadius.circular(10)),
                              child: ListTile(
                                leading: Image.network(e.image),
                                title: Container(
                                  width: 200,
                                  child: Text(
                                    e.name,
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                                subtitle: Text(
                                  "ใช้ ${e.price * 15} คะแนน",
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                                trailing: Text(
                                  "กดแลก",
                                  style: TextStyle(color: Colors.green),
                                ),
                              ),
                            ),
                          ))
                      .toList(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
