import 'package:chatbot/models/Account.dart';
import 'package:chatbot/models/Order.dart';
import 'package:chatbot/screens/LoginPage.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'GiftProduct.dart';
import 'PromotionProduct.dart';
import 'Waiting.dart';

class EndDrawer extends StatefulWidget {
  const EndDrawer({
    Key key,
  }) : super(key: key);

  @override
  _EndDrawerState createState() => _EndDrawerState();
}

class _EndDrawerState extends State<EndDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 130,
              color: Theme.of(context).primaryColor,
              child: Row(
                children: [
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(25.0),
                        child: Container(
                          height: 100,
                          child: Wrap(
                            direction: Axis.horizontal,
                            alignment: WrapAlignment.spaceEvenly,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            spacing: 16,
                            runSpacing: 20,
                            children: [
                              CircleAvatar(
                                backgroundColor: Colors.white,
                                child: Icon(
                                  Icons.account_circle,
                                  size: 30,
                                  color: Theme.of(context).primaryColor,
                                ),
                              ),
                              Account.username != null
                                  ? Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text("${Account.username}",
                                            style: TextStyle(fontSize: 20)),
                                        Text("${Account.phone}",
                                            style: TextStyle(fontSize: 20)),
                                      ],
                                    )
                                  : Center(
                                      child: Text("ผู้ใช้งานทั่วไป"),
                                    )
                            ],
                          ),
                        ),
                      ),
                    ],
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  )
                ],
              ),
            ),
            Account.id != null
                ? FlatButton(
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext ctx) {
                            return Dialog(child: GiftProduct());
                          });
                    },
                    child: ListTile(
                      leading: Icon(
                        FontAwesomeIcons.award,
                        size: 30,
                      ),
                      title: Text("แลกคะแนน"),
                    ),
                  )
                : SizedBox(),
            Account.id != null
                ? FlatButton(
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return OrderDialog();
                          });
                    },
                    child: ListTile(
                      leading: Icon(
                        Icons.history,
                        size: 30,
                      ),
                      title: Text("คำสั่งซื้อของฉัน"),
                    ),
                  )
                : SizedBox(),
            FlatButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => PromotionProduct()));
              },
              child: ListTile(
                leading: Icon(
                  Icons.fastfood,
                  size: 30,
                ),
                title: Text("โปรโมชั่น"),
              ),
            ),
            FlatButton(
              onPressed: () async {
                Account.id != null
                    ? await Account.signOut(context)
                    : Navigator.of(context).push(
                        MaterialPageRoute(builder: (BuildContext context) {
                        return LoginPage();
                      }));
              },
              child: ListTile(
                leading: Icon(
                  Account.id != null
                      ? Icons.exit_to_app
                      : Icons.arrow_forward_ios,
                  size: 30,
                ),
                title: Text(Account.id != null ? "Logout" : "Login"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class OrderDialog extends StatefulWidget {
  const OrderDialog({
    Key key,
  }) : super(key: key);

  @override
  _OrderDialogState createState() => _OrderDialogState();
}

class _OrderDialogState extends State<OrderDialog> {
  int indexStack = 0;

  @override
  void initState() {
    getMyOrder();
    super.initState();
  }

  Future<void> getMyOrder() async {
    await Order.getOrder();
    await Future.delayed(Duration(seconds: 2));
    setState(() {
      indexStack = 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return IndexedStack(
      index: indexStack,
      children: [
        Dialog(child: Container(height: 450, child: Waiting())),
        Dialog(
          insetAnimationDuration: Duration(seconds: 1),
          child: Container(
            height: 450,
            child: ListView(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 3),
                  height: 40,
                  color: Theme.of(context).primaryColor,
                  child: Text(
                    "คำสั่งซื้อของฉัน",
                    textAlign: TextAlign.center,
                  ),
                ),
                Column(
                  children: Order.orderLst.map((e) {
                    return ExpansionTile(
                      leading: Text(
                        "รหัสคำสั่งซื้อ :${e.id.toString()}",
                        style: TextStyle(color: Colors.black),
                      ),
                      title: Text(
                        "มูลค่า :${e.sum.toString()} บาท",
                        style: TextStyle(color: Colors.black),
                      ),
                      children: e.detail
                          .map((ed) => ListTile(
                                leading: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.5,
                                  child: Text(
                                    ed.name,
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 10),
                                  ),
                                ),
                                title: Text(
                                  ed.quantity.toString() + "ชิ้น",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 10),
                                ),
                                subtitle: Text(
                                  ed.price.toString() + "บาท",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 10),
                                ),
                              ))
                          .toList(),
                    );
                  }).toList(),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
