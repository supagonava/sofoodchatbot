import 'package:flutter/material.dart';
import 'screens/Homepage.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Sofood',
      theme: ThemeData(
        primaryColor: Color.fromRGBO(255, 206, 41, 1),
        fontFamily: 'Prompt',
        
        textTheme: TextTheme(
          headline1: TextStyle(
            fontSize: 20,
            color: Colors.white,
          ),
          headline2: TextStyle(fontSize: 16, color: Colors.black54),
          bodyText1: TextStyle(fontSize: 14, color: Color(0xff303030)),
          bodyText2: TextStyle(fontSize: 14, color: Colors.white),
          caption: TextStyle(fontSize: 12, color: Color(0xff303030)),
          button: TextStyle(fontSize: 16, color: Color(0xff303030)),
        ),
      ),
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}
