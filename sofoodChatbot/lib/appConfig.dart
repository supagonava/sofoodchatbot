import 'package:shared_preferences/shared_preferences.dart';
// import 'model/Tool.dart';
// import 'model/User.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
// import 'model/Company.dart';
import 'package:intl/intl.dart';

class AppConfig {
  static String host = "http://202.183.198.250:5000/";
  static String api = host + "";
  static String pushToCartApi = host + "cart";
  static String getCartApi = "cart/user/";
  static String signinApi = host + "users/login";
  static String getAllProdApi = host + "product";
  static String decryptTokenApi = api + "users/token";
  static String getAllToolApi = api + "tool/get-tools";
  static String loginApi = api + "users/signout";
  static String insertOrder = api + "order/make-order";
  static String getOrdersApi = api + "order/get-orders";
  static String cancelOrderUrl = api + "order/cancel-order";
  static String updateUser = api + "users/update-users";
  static String findTool = api + "tools/find-tool";
  static SharedPreferences prefs;
  static String token;
  static Map userPosition = {'lat': null, 'lng': null};
  static var header = {"Content-Type": "application/json"};

  static String searchTxt = '';

  static final formatCurrency = NumberFormat.currency(locale: 'th', symbol: '');
  static var areaSizeList = <String>[
    '0-20 ไร่',
    '21-50 ไร่',
    '50 ไร่ขึ้นไป',
  ];
// 1 => '0-20 ไร่', 2 => '21-50 ไร่', 3 => '50 ไร่ขึ้นไป',4=>'0-10 ตัน',5=>'11-30 ตัน',6=>'30 ตันขึ้นไป'
  static var serviceTypeList = <String>['ซื้อขาด', 'เช่า', 'บริการ'];

  static var typeList = <String>[
    'เตรียมแปลง',
    'ดูแลรักษา',
    'เก็บเกี่ยว/หลังเก็บเกี่ยว'
  ];
  static setToken() async {
    prefs = await SharedPreferences.getInstance();
    token = (prefs.getString('token').toString());
  }

  static positionToAddress(var position) {
    return ("https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.latitude},${position.longitude}&sensor=true&language=th&key=AIzaSyDo3ANBjlvuxeCfxdkJzgZ5tUjtoXbGtbM");
  }

  static addressToPosition(var address) {
    return ("https://maps.googleapis.com/maps/api/geocode/json?address=$address&sensor=false&key=AIzaSyDo3ANBjlvuxeCfxdkJzgZ5tUjtoXbGtbM&language=th");
  }

  static post(var url, var data) async {
    var res = await http.post(url, headers: header, body: data);
    if (res.statusCode == 200) {
      var response = convert.jsonDecode(res.body);
      print(url);
      print(data);
      print(response);
      return response;
    } else {
      return convert.jsonDecode('''{"message": "error"}''');
    }
  }

  // static Company company = Company(
  //     id: null, name: null, phone: null, email: null, lat: null, lng: null);
  // static User user = User(id: null, phone: null, fname: null, lname: null);
}
