<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SignupForm;
use app\components\myRule;
use app\models\ActionLog;
use app\models\ChatLog;
use app\models\User;

class SiteController extends Controller
{
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','index'],
                'ruleConfig' => [
                    'class' => myRule::className() // เรียกใช้งาน accessRule (component) ที่เราสร้างขึ้นใหม่
                ],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionGetChat(){
        $data = json_decode(file_get_contents("php://input"));

        $model = ChatLog::find()->where(["user_id"=>$data->user_id])->asArray()->all();
        if(!empty($model)){
            return $this->asJson(['status'=>true,'data'=>$model]);
        }else{
            return $this->asJson(['status'=>false,'data'=>null]);
        }
    }

    public function actionChatLog(){
        $data = json_decode(file_get_contents("php://input"));

        $model = new ChatLog();
        $model->user_id = $data->user_id;
        $model->chat_message = $data->chat_message;
        $model->chat_response = $data->chat_response;
        $model->intent = $data->intent;
        $model->time = date("Y-m-d H:i:s");
        if($model->save()){
            return $this->asJson(['status'=>true,'message'=>'successful inserted']);
        }else{
            return $this->asJson(['status'=>false,'message'=>$model->getErrors()]);
        }
    }

    public function actionActionLog(){
        $data = json_decode(file_get_contents("php://input"));
        $model = new ActionLog();
        $model->user_id = $data->user_id;
        $model->action = $data->action;
        $model->product_id = $data->product_id;
        if($model->save()){
            return $this->asJson(['status'=>true,'message'=>'successful inserted']);
        }else{
            return $this->asJson(['status'=>false,'message'=>$model->getErrors()]);
        }
    }
}
