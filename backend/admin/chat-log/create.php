<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ChatLog */

$this->title = 'Create Chat Log';
$this->params['breadcrumbs'][] = ['label' => 'Chat Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chat-log-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
