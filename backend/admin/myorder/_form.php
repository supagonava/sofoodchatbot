<?php

use app\models\Users;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Myorder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="myorder-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $model= Users::find()->all();
    $objItem = ArrayHelper::map($model, 'id','username');?>
    
    <?= $form->field($model, 'user_id')->dropDownList($objItem) ?>

    <!-- <?= $form->field($model, 'status')->textInput() ?> -->

    <?= $form->field($model, 'shipping_address')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
