<?php

use yii\helpers\Url;
use yii\bootstrap4\Html;
use app\models\Product;
use app\models\ActionLog;

$this->title = 'หน้าหลักของระบบ';
$this->params['breadcrumbs'] = [['label' => $this->title]];
?>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<div class="row">
    <a href="<?= Url::base() . "/users" ?>" class="col-xs-6 col-md-4">
        <div class="btn btn-success" style="width: 100%">
            <div class="card-body row">
                <div class="col text-center mb-3">
                    <i style="font-size: 30px;padding: 10px" class=" fas fa-user"></i>
                </div>
                <div class="col text-center">
                    <p>ผู้ใช้งาน</p>
                    <p>
                        <?= Yii::$app->db->createCommand("SELECT count(id) as count from users")->queryOne()['count'] ?> คน
                    </p>
                </div>
            </div>
        </div>
    </a>

    <a href="<?= Url::base() . "/myorder" ?>" class="col-xs-6 col-md-4 ">
        <div class="btn btn-warning" style="width: 100%">
            <div class="card-body row">
                <div class="col text-center mb-3">
                    <i style="font-size: 30px;padding: 10px" class=" fas fa-money"></i>
                </div>
                <div class="col text-center">
                    <p>รายการสั่งซื้อ</p>
                    <p>
                        <?= Yii::$app->db->createCommand("SELECT count(id) as count from myorder")->queryOne()['count'] ?> รายการ
                    </p>
                </div>
            </div>
        </div>
    </a>
    <a href="<?= Url::base() . "/product" ?>" class="col-xs-6 col-md-4">
        <div class="btn btn-primary" style="width: 100%">
            <div class="card-body row">
                <div class="col text-center mb-3">
                    <i style="font-size: 30px;padding: 10px" class=" fa fa-shopping-bag"></i>
                </div>
                <div class="col text-center">
                    <p>สินค้า</p>
                    <p>
                        <?= Yii::$app->db->createCommand("SELECT count(id) as count from product")->queryOne()['count'] ?> รายการ
                    </p>
                </div>
            </div>
        </div>
    </a>
</div>
<div class="row">
    <div class="col-md-8" style="margin-top: 1rem;margin-bottom: 1rem;">
        <div id="user-log">
            <script>
                Highcharts.chart('user-log', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'พฤติกรรมของผู้ใช้งาน'
                    },

                    accessibility: {
                        announceNewData: {
                            enabled: true
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    xAxis: {
                        type: 'category'
                    },
                    yAxis: {
                        title: {
                            text: 'จำนวนคลิกทั้งหมด'
                        }
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                                format: '{point.y}'
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y} ครั้ง</b><br/>'
                    },

                    series: [{
                        name: "เข้า",
                        colorByPoint: true,
                        data: [
                            <?php
                            $model = Yii::$app->db->createCommand("SELECT *,COUNT(action_log.action) as actionCount,COUNT(action_log.product_id) AS prodAction FROM action_log LEFT JOIN product ON product_id = product.id GROUP BY product_id,action")->queryAll();
                            foreach ($model as $log) : ?> {
                                    name: "<?= $log['action'] . ' ' . $log['name'] ?>",
                                    y: <?= $log['actionCount'] ?>
                                },
                            <?php endforeach; ?>
                        ]
                    }],
                });
            </script>
        </div>
    </div>

    <div class="col-md-4" style="margin-top: 15px;">
        <div class="card box-danger box-solid" style="padding: 15px;">
            <div class="box-header with-border">
                <div class="box-title">สินค้าที่ได้รับความสนใจสูงสูด</div>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="box-body no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>สินค้า</th>
                                <th>จำนวนเข้าชม</th>
                                <th>ตรวจสอบ</th>
                            </tr>
                            <?php $model = Yii::$app->db->createCommand("SELECT product.id,product.name,COUNT(product_id) as count FROM action_log LEFT JOIN product ON action_log.product_id = product.id GROUP BY product.id ORDER BY COUNT desc LIMIT 5")->queryAll();
                            foreach ($model as $item) : ?>
                                <tr>
                                    <td><?= $item['name'] ?></td>
                                    <td><?= $item['count'] ?></span></td>
                                    <td><?= Html::a('ดู', Url::base() . "/product/view?id=" . $item['id'], $options = ['class' => 'btn btn-info text-white']) ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box-body -->
        </div>
    </div>


    <div class="col-md-12" style="margin-top: 1rem;margin-bottom: 1rem;">
        <div id="chat-log">
            <script>
                Highcharts.chart('chat-log', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'การเรียกใช้งานน้องจูเลียต'
                    },

                    accessibility: {
                        announceNewData: {
                            enabled: true
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    xAxis: {
                        type: 'category'
                    },
                    yAxis: {
                        title: {
                            text: 'จำนวนคลิกทั้งหมด'
                        }
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                                format: '{point.y}'
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y} ครั้ง</b><br/>'
                    },

                    series: [{
                        name: "เข้า",
                        colorByPoint: true,
                        data: [
                            <?php
                            $model = Yii::$app->db->createCommand("SELECT *,COUNT(chat_log.intent) as intentCount FROM chat_log GROUP BY intent")->queryAll();
                                 foreach ($model as $log) : ?> {
                                    name: "<?= $log['intent']?>",
                                    y: <?= $log['intentCount'] ?>
                                },
                            <?php endforeach; ?>
                        ]
                    }],
                });
            </script>
        </div>
    </div>

</div>