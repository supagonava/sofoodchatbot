<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property int $price
 * @property string|null $path
 * @property int|null $category
 *
 * @property ActionLog[] $actionLogs
 * @property Cart[] $carts
 * @property OrderDetail[] $orderDetails
 * @property ProductPromotion[] $productPromotions
 * @property Promotion[] $promotions
 * @property Vote[] $votes
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'price'], 'required'],
            [['description'], 'string'],
            [['price', 'category'], 'integer'],
            [['name'], 'string', 'max' => 200],
            [['path'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'price' => 'Price',
            'path' => 'Path',
            'category' => 'Category',
        ];
    }

    /**
     * Gets query for [[ActionLogs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActionLogs()
    {
        return $this->hasMany(ActionLog::className(), ['product_id' => 'id']);
    }

    /**
     * Gets query for [[Carts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCarts()
    {
        return $this->hasMany(Cart::className(), ['prod_id' => 'id']);
    }

    /**
     * Gets query for [[OrderDetails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDetails()
    {
        return $this->hasMany(OrderDetail::className(), ['product_id' => 'id']);
    }

    /**
     * Gets query for [[ProductPromotions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductPromotions()
    {
        return $this->hasMany(ProductPromotion::className(), ['product_id' => 'id']);
    }

    /**
     * Gets query for [[Promotions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPromotions()
    {
        return $this->hasMany(Promotion::className(), ['id' => 'promotion_id'])->viaTable('product_promotion', ['product_id' => 'id']);
    }

    /**
     * Gets query for [[Votes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVotes()
    {
        return $this->hasMany(Vote::className(), ['product_id' => 'id']);
    }
}
