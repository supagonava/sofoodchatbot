<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "chat_log".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $chat_message
 * @property string $chat_response
 * @property string $time
 * @property string $intent
 *
 * @property Users $user
 */
class ChatLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chat_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['chat_message', 'chat_response', 'intent'], 'required'],
            [['chat_message', 'chat_response'], 'string'],
            [['time'], 'safe'],
            [['intent'], 'string', 'max' => 45],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'chat_message' => 'Chat Message',
            'chat_response' => 'Chat Response',
            'time' => 'Time',
            'intent' => 'Intent',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
