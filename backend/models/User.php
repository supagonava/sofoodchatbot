<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string|null $auth_key
 * @property string $password_hash
 * @property int $roles
 * @property string $email
 * @property int $status
 */

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    const ROLE_DEVELOPER = 0;
    const ROLE_USER = 10;
    const ROLE_ADMIN = 30;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin';
    }

    /**
     * @inheritdoc
     */
    /*Pongpon ปรับแต่ง rules ให้กับ field role */
    public function rules()
    {
        return [

            //Pongpon เพิ่ม role
            ['roles', 'default', 'value' => self::ROLE_USER],
            ['roles', 'in', 'range' => [self::ROLE_DEVELOPER, self::ROLE_USER, self::ROLE_ADMIN]],

            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],

            [['username', 'password_hash', 'email', 'status'], 'required'],
            [['roles', 'status'], 'integer'],
            [['username', 'email'], 'string', 'max' => 50],
            [['auth_key'], 'string', 'max' => 100],
            [['password_hash'], 'string', 'max' => 200],
        ];
    }
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'roles' => 'Roles',
            'email' => 'Email',
            'status' => 'Status',
        ];
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);

        return $this->password_hash == $password;
    }

    public static function findIdentity($id)
    {
        return self::findOne($id);
    }
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }


    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public static function getRoleName($roleID)
    {
        $roles = array(
            0 => 'นักพัฒนา',
            30 => 'ผู้ดูแลระบบ',
            10 => 'ผู้ใช้งานทั่วไป',
            // 20 => 'พนักงาน',
            // 40 => 'ลูกค้า',
        );
        if (!empty($roles[$roleID])) {
            return $roles[$roleID];
        } else {
            return $roles;
        }
    }

    public static function getStatusName($status)
    {
        $array = array(
            0 => "Deactive",
            1 => "Active",
        );
        if (!empty($array[$status])) {
            return $array[$status];
        } else {
            return "ไม่ระบุสถานะ";
        }
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
}
