<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string|null $firstname
 * @property string|null $lastname
 * @property string|null $date_of_birth
 * @property string|null $gender
 * @property string|null $phone
 * @property string|null $career
 * @property string|null $province
 * @property string $username
 * @property string $password
 * @property int $point
 *
 * @property ActionLog[] $actionLogs
 * @property Cart[] $carts
 * @property ChatLog[] $chatLogs
 * @property Myorder[] $myorders
 * @property Vote[] $votes
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_of_birth'], 'safe'],
            [['username', 'password'], 'required'],
            [['point'], 'integer'],
            [['firstname', 'lastname', 'career', 'username'], 'string', 'max' => 45],
            [['gender'], 'string', 'max' => 1],
            [['phone'], 'string', 'max' => 10],
            [['province'], 'string', 'max' => 50],
            [['password'], 'string', 'max' => 100],
            [['username'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'date_of_birth' => 'Date Of Birth',
            'gender' => 'Gender',
            'phone' => 'Phone',
            'career' => 'Career',
            'province' => 'Province',
            'username' => 'Username',
            'password' => 'Password',
            'point' => 'Point',
        ];
    }

    /**
     * Gets query for [[ActionLogs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActionLogs()
    {
        return $this->hasMany(ActionLog::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[Carts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCarts()
    {
        return $this->hasMany(Cart::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[ChatLogs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChatLogs()
    {
        return $this->hasMany(ChatLog::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[Myorders]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMyorders()
    {
        return $this->hasMany(Myorder::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[Votes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVotes()
    {
        return $this->hasMany(Vote::className(), ['user_id' => 'id']);
    }
}
