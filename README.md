# README #

โปรเจคทำแอปแชทบอท So food สำหรับการแข่งขัน Cpf hackathond 2020

### Tech ###
* Mobile App
	Flutter
* Chatbot engine
	Dialogflow
* backend
	php (admin management)
	nodejs(api)

### Our Idea ###
Sofood จะเป็นแชทบอทบนแอปพลิเคชั่นมือถือ ที่คอยตอบคำถามและปัญหาต่างๆ 
และนอกจากนี้ Sofood จะเป็นผู้แนะนำสินค้าจากร้านค้าปลีกย่อยในกรณีที่ผู้ใช้งานต้องการคำแนะนำเกี่ยวกับการเลือกซื้อ
รวมถึงบอกรายละเอียดของสินค้านั้นๆได้ดี และถ้าหากว่าทางร้านค้ามีการทำโปรโมชั่น ตัว Sofood เองก็จะแจ้งไปยังผู้ใช้งานเพื่อไม่ให้ผู้ใช้งานพลาดสิ่งที่ทางร้านจะนำเสนอด้วย

และในระบบหลังบ้านสำหรับผู้จัดการร้านค้าเองก็จะเห็นว่ามีข้อมูล(ในลักษณะ กราฟต่างๆ)ใดบ้างที่ผู้ใช้งานหรือลูกค้าทำการพูดคุยกับตัวแชทบอทบ่อยๆ เพื่อนำไปปรับปรุงการให้บริการของร้าน
รวมถึงการตามความคิดของลูกค้าทัน และจะมีการเสนอโปรโมชั่นที่ทางร้านควรจัดเนื่องจากกำลังเป็นจุดสนใจของลูกค้าด้วย
